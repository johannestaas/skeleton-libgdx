package com.fusion.skeleton;

import com.badlogic.gdx.Game;
import com.fusion.skeleton.screens.SplashScreen;

public class SkeletonGame extends Game {

	private SplashScreen splashScreen;
	
	@Override
	public void create() {
		splashScreen = new SplashScreen(this);
		this.setScreen(splashScreen);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
